#include <QApplication>
#include <cinolib/meshes/meshes.h>
#include <cinolib/gui/qt/glcanvas.h>
#include <cinolib/gui/qt/surface_mesh_control_panel.h>

#include "control_panel.h"

using namespace cinolib;

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    DrawableTrimesh<> m;
    if (argc > 1) m = DrawableTrimesh<>(argv[1]);

    GLcanvas gui;
    gui.push_obj(&m);
    gui.show();

    ControlPanel<DrawableTrimesh<>> panel(&m, &gui);
    panel.show();

    m.show_wireframe(true);

    return a.exec();
}
