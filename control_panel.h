#ifndef CONTROL_PANEL_H
#define CONTROL_PANEL_H

#include <cinolib/gui/qt/surface_mesh_control_panel.h>
#include <cinolib/bfs.h>
#include <cinolib/heat_flow.h>
#include <cinolib/geodesics.h>
#include <QDoubleSpinBox>

using namespace cinolib;

template<class Mesh>
class ControlPanel : public SurfaceMeshControlPanel<Mesh>
{
    public:

        ControlPanel(Mesh *m, GLcanvas *canvas, QWidget *parent = NULL);

    protected:

        void connect();

        QPushButton    *but_labeling;
        QSpinBox       *sb_seed_0;
        QSpinBox       *sb_seed_1;
        QPushButton    *but_heat_flow;
        QSpinBox       *sb_heat_time;
        QPushButton    *but_init_field;
        QPushButton    *but_orient_field;
        QPushButton    *but_smooth_field;
        QSpinBox       *sb_niters;
        QDoubleSpinBox *sb_lambda;
        QPushButton    *but_integrate;
};

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

template<class Mesh>
ControlPanel<Mesh>::ControlPanel(Mesh *m, GLcanvas *canvas, QWidget *parent) :
    SurfaceMeshControlPanel<Mesh>(m, canvas, parent)
{
    QGroupBox *gbox  = new QGroupBox("Boundary Smoothing", parent);
    but_labeling     = new QPushButton("Labeling", gbox);
    sb_seed_0        = new QSpinBox(parent);
    sb_seed_1        = new QSpinBox(parent);
    but_heat_flow    = new QPushButton("Heat Flow", gbox);
    sb_heat_time     = new QSpinBox(parent);
    but_init_field   = new QPushButton("Initialize Field", gbox);
    but_orient_field = new QPushButton("Orient Field", gbox);
    but_smooth_field = new QPushButton("Smooth Field", gbox);
    sb_niters        = new QSpinBox(parent);
    but_integrate    = new QPushButton("Integrate", gbox);
    sb_lambda        = new QDoubleSpinBox(parent);
    QLabel *l_seed_0 = new QLabel("Seed #0:");
    QLabel *l_seed_1 = new QLabel("Seed #1:");
    QLabel *l_time   = new QLabel("Time step x:");
    QLabel *l_iters  = new QLabel("Iterations:");
    QLabel *l_lambda = new QLabel("Lambda:");
    gbox->setFont(this->global_font);
    but_labeling->setFont(this->global_font);
    sb_seed_0->setFont(this->global_font);
    sb_seed_1->setFont(this->global_font);
    but_heat_flow->setFont(this->global_font);
    sb_heat_time->setFont(this->global_font);
    but_init_field->setFont(this->global_font);
    but_orient_field->setFont(this->global_font);
    but_smooth_field->setFont(this->global_font);
    sb_niters->setFont(this->global_font);
    but_integrate->setFont(this->global_font);
    sb_lambda->setFont(this->global_font);
    l_seed_0->setFont(this->global_font);
    l_seed_1->setFont(this->global_font);
    l_time->setFont(this->global_font);
    l_iters->setFont(this->global_font);
    l_lambda->setFont(this->global_font);
    QGridLayout *layout = new QGridLayout();
    layout->addWidget(l_seed_0,0,0);
    layout->addWidget(sb_seed_0,0,1);
    layout->addWidget(l_seed_1,1,0);
    layout->addWidget(sb_seed_1,1,1);
    layout->addWidget(but_labeling,2,0,1,2);
    layout->addWidget(l_time,3,0);
    layout->addWidget(sb_heat_time,3,1);
    layout->addWidget(but_heat_flow,4,0,1,2);
    layout->addWidget(but_init_field,5,0,1,2);
    layout->addWidget(but_orient_field,6,0,1,2);
    layout->addWidget(l_iters,7,0);
    layout->addWidget(sb_niters,7,1);
    layout->addWidget(but_smooth_field,8,0,1,2);
    layout->addWidget(l_lambda,9,0);
    layout->addWidget(sb_lambda,9,1);
    layout->addWidget(but_integrate,10,0,1,2);
    gbox->setLayout(layout);
    this->global_layout->addWidget(gbox,0,2,3,1);

    sb_seed_1->setMaximum(m->num_verts());
    sb_seed_1->setValue(m->num_verts()-1);
    sb_heat_time->setValue(1);
    sb_niters->setValue(3);
    sb_niters->setMaximum(100);
    sb_lambda->setValue(0.2);

    connect();
}

//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

template<class Mesh>
void ControlPanel<Mesh>::connect()
{
    QPushButton::connect(but_labeling, &QPushButton::clicked, [&]()
    {
        // STEP ZERO: start with some bipartition of the domain
        uint s0 = sb_seed_0->value();
        uint s1 = sb_seed_1->value();

        std::vector<double> dist_0;
        std::vector<double> dist_1;
        bfs_exahustive_on_dual(*this->m, s0, dist_0);
        bfs_exahustive_on_dual(*this->m, s1, dist_1);

        ScalarField labeling(this->m->num_polys());
        for(uint pid=0; pid<this->m->num_polys(); ++pid)
        {
            int label = (dist_0.at(pid) <= dist_1.at(pid)) ? 0 : 1;
            this->m->poly_data(pid).label = label;
            this->m->poly_data(pid).color = (label==0) ? Color::PASTEL_YELLOW() : Color::PASTEL_CYAN();
            labeling[pid] = label;
        }
        labeling.serialize("labeling");
        std::cout << "labeling attached to mesh" << std::endl;
        this->rb_poly_color->toggled(true);
    });

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    QPushButton::connect(but_heat_flow, &QPushButton::clicked, [&]()
    {
        // STEP ONE: heat flow
        std::vector<uint> heat_sources;
        for(uint vid=0; vid<this->m->num_verts(); ++vid)
        {
            bool has_A = false;
            bool has_B = false;
            for(uint pid : this->m->adj_v2p(vid))
            {
                if (this->m->poly_data(pid).label == 0) has_A = true; else
                if (this->m->poly_data(pid).label == 1) has_B = true;
            }
            if (has_A && has_B) heat_sources.push_back(vid);
        }
        double t = this->m->edge_avg_length();
        t *= t;
        t *= static_cast<double>(sb_heat_time->value());
        ScalarField u = heat_flow(*this->m, heat_sources, t, COTANGENT);
        u.normalize_in_01();
        u.copy_to_mesh(*this->m);
        u.serialize("u");
        std::cout << "heat flow computed" << std::endl;
        this->cb_tex1D_type->setCurrentIndex(TEXTURE_1D_HSV_RAMP_W_ISOLINES);
        this->cb_tex1D_type->activated(TEXTURE_1D_HSV_RAMP_W_ISOLINES);
    });

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    QPushButton::connect(but_init_field, &QPushButton::clicked, [&]()
    {
        // STEP TWO.1: start with the gradient of one of the regions...
        this->cb_gradient->setChecked(true);
        this->gradient.serialize("u_gradient");
        std::cout << "u gradient computed" << std::endl;
    });

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    QPushButton::connect(but_orient_field, &QPushButton::clicked, [&]()
    {
        // STEP TWO.2: and flip it on one of them...
        for(uint pid=0; pid<this->m->num_polys(); ++pid)
        {
            if (this->m->poly_data(pid).label == 1) this->gradient.set(pid, -this->gradient.vec_at(pid));
        }
        this->gradient.normalize();
        this->gradient.serialize("X");
        std::cout << "X field generated" << std::endl;
        this->canvas->updateGL();
    });

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    QPushButton::connect(but_smooth_field, &QPushButton::clicked, [&]()
    {
        // STEP THREE: smooth the resulting gradient
        for(int i=0; i<sb_niters->value(); ++i)
        for(uint pid=0; pid<this->m->num_polys(); ++pid)
        {
            vec3d avg_g = this->gradient.vec_at(pid);
            for(uint nbr : this->m->adj_p2p(pid))
            {
                avg_g += this->gradient.vec_at(nbr);
            }
            avg_g /= static_cast<double>(this->m->adj_p2p(pid).size()+1);
            avg_g.normalize();
            this->gradient.set(pid,avg_g);
        }
        this->gradient.normalize();
        this->gradient.serialize("X_prime");
        std::cout << "smoothed X field generated" << std::endl;
        this->canvas->updateGL();
    });

    //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    QPushButton::connect(but_integrate, &QPushButton::clicked, [&]()
    {
        // STEP FOUR: find the scalar field corresponding to it
        std::vector<uint> heat_sources;
        for(uint vid=0; vid<this->m->num_verts(); ++vid)
        {
            bool has_A = false;
            bool has_B = false;
            for(uint pid : this->m->adj_v2p(vid))
            {
                if (this->m->poly_data(pid).label == 0) has_A = true; else
                if (this->m->poly_data(pid).label == 1) has_B = true;
            }
            if (has_A && has_B) heat_sources.push_back(vid);
        }

        std::vector<Eigen::Triplet<double>> entries = laplacian_matrix_entries(*this->m, COTANGENT);
        Eigen::VectorXd tmp = gradient_matrix(*this->m).transpose() * this->gradient;
        Eigen::SparseMatrix<double> L(this->m->num_verts()+heat_sources.size(), this->m->num_verts());
        Eigen::VectorXd rhs(this->m->num_verts()+heat_sources.size());
        for(uint vid=0; vid<this->m->num_verts(); ++vid) rhs[vid] = tmp[vid];

        double lambda = sb_lambda->value();
        for(uint i=0; i<heat_sources.size(); ++i)
        {
            uint vid = heat_sources.at(i);
            entries.push_back(Entry(this->m->num_verts()+i, vid, lambda * 1.0));
            rhs[this->m->num_verts()+i] = lambda * 0.0;
        }
        L.setFromTriplets(entries.begin(), entries.end());
        ScalarField phi;
        solve_least_squares(-L, rhs, phi);
        phi.normalize_in_01();
        phi.copy_to_mesh(*this->m);
        phi.serialize("phi");
        std::cout << "phi scalar function computed" << std::endl;
        this->cb_tex1D_type->setCurrentIndex(TEXTURE_1D_HSV_RAMP_W_ISOLINES);
        this->cb_tex1D_type->activated(TEXTURE_1D_HSV_RAMP_W_ISOLINES);
        this->m->updateGL();
        this->rb_poly_color->setChecked(true);
        this->cb_gradient->setChecked(false);
        this->cb_isocurve->setChecked(true);
        this->canvas->updateGL();
    });
}

#endif // CONTROL_PANEL_H
