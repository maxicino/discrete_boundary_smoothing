QT             += core opengl xml
CONFIG         += c++11
TARGET          = discrete_boundary_smoothing
TEMPLATE        = app
CONFIG         -= app_bundle
DEFINES        += CINOLIB_PROFILER_ENABLED
QMAKE_CXXFLAGS += -Wno-deprecated-declarations # kill glu deprecated warnings...
INCLUDEPATH    += /usr/local/include
INCLUDEPATH    += /Users/cino/Documents/research/devel/lib/CinoLib/include

# enable OpenGL and QGLViewer (used in cinolib/gl/glcanvas.cpp)
DEFINES      += CINOLIB_USES_OPENGL
DEFINES      += CINOLIB_USES_QT_AND_QGLVIEWER
INCLUDEPATH  += /Library/Frameworks/QGLViewer.framework/Versions/2/Headers
QMAKE_LFLAGS *= -F/Library/Frameworks
LIBS         += -framework QGLViewer

SOURCES += main.cpp
HEADERS += control_panel.h
